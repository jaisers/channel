# CNCN - S01E02 - 2020-07-21

- [Video](https://youtu.be/CvsVo6pV68Q)

## Today's Topics

- Rancher releases k3d v3.0.0 - https://llb.io/n9vur
- RanchCast deep dive into k3d - https://llb.io/9zevg
- Which K8s Certification is Right for You? - https://llb.io/pg92m
- Linux Foundation Developing Certificate for Certified Kubernetes Security Specialist (CKS) - https://llb.io/26dns
- Backup and Restore of MongoDB on K8s - https://llb.io/7cg0n
- Yugabyte DB Releases v2.2 - https://llb.io/qb7hx
- Cinderella Clusters with K3s - https://llb.io/prhlw

## Tool of the Day

- Kubetap - https://llb.io/j0itx
- Kubetap, Behind the Scenes - https://llb.io/wokwt
